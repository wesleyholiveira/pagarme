<?php

namespace App\Http\Controllers;

use App\Repository\FantasiaRepository;
use Illuminate\Http\Request;

class FantasiaController extends Controller
{
    protected $fantasia;

    public function __construct(FantasiaRepository $fantasia)
    {
        $this->fantasia = $fantasia;
    }

    public function get($id = null)
    {
        return $this->fantasia->findAll();
    }

    public function post(Request $request)
    {

    }

}