<?php

namespace App\Http\Controllers;

use App\Factories\FornecedorFactory;
use App\Repository\FornecedorRepository;
use Fig\Http\Message\StatusCodeInterface;
use Illuminate\Http\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class FornecedorController extends Controller
{
    /* @var FornecedorFactory */
    protected $fornecedorFactory;

    /* @var FornecedorRepository */
    protected $fornecedor;

    public function __construct(FornecedorRepository $fornecedor, FornecedorFactory $fornecedorFactory)
    {
        $this->fornecedor = $fornecedor;
        $this->fornecedorFactory = $fornecedorFactory;
    }

    public function get(int $id = null)
    {
        try {
            $response = null;

            if (isset($id) && is_integer($id)) {
                $response = $this->fornecedor->buscar($id);
            } else {
                $response = $this->fornecedor->findAll();
            }
            return response()->json(
                $response,
                StatusCodeInterface::STATUS_OK
            );
        } catch (NotFoundHttpException $e) {
            return response()->json(
                ['descricao' => $e->getMessage()],
                $e->getCode()
            );
        }
    }

    public function post(Request $request)
    {
        try {
            $data = $request->all();
            $factory = $this->fornecedorFactory;
            $fornecedor = $factory($data['nome'], $data['comissao']);
            $this->fornecedor->criar($fornecedor);
        } catch (Exception $e) {

        }
    }

}