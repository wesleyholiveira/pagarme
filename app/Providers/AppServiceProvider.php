<?php

namespace App\Providers;

use App\Entities\FantasiaEntity;
use App\Entities\FornecedorEntity;
use App\Factories\FantasiaFactory;
use App\Factories\FornecedorFactory;
use App\Repository\FantasiaRepository;
use App\Repository\FornecedorRepository;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(FornecedorRepository::class, function() {
            return new FornecedorRepository(
                $this->app['em'],
                $this->app['em']->getClassMetaData(FornecedorEntity::class)
            );
        });

        $this->app->bind(FantasiaRepository::class, function() {
            return new FantasiaRepository(
                $this->app['em'],
                $this->app['em']->getClassMetaData(FantasiaEntity::class)
            );
        });

        $this->app->bind(FornecedorFactory::class, function() {
           return new FornecedorFactory();
        });

        $this->app->bind(FantasiaFactory::class, function() {
            return new FantasiaFactory();
        });
    }
}
