<?php

namespace App\Factories;

use App\Entities\FornecedorEntity;

class FornecedorFactory
{
    public function __invoke(string $nome, float $comissao)
    {
        return new FornecedorEntity($nome, $comissao);
    }
}
