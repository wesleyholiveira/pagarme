<?php

namespace App\Factories;

use App\Entities\FantasiaEntity;

class FantasiaFactory
{
    public function __invoke(string $descricao, float $valor)
    {
        return new FantasiaEntity($descricao, $valor);
    }
}
