<?php

namespace App\Repository;

use App\Entities\AbstractEntity;

interface DefaultCRUDRepository
{
    public function buscar(int $id);
    public function criar(AbstractEntity $entidade);
    public function atualizar(AbstractEntity $entidade);
    public function deletar(AbstractEntity $entidade);
}