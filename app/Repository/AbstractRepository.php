<?php

namespace App\Repository;

use App\Entities\AbstractEntity;
use Doctrine\ORM\EntityRepository;
use Fig\Http\Message\StatusCodeInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class AbstractRepository extends EntityRepository implements DefaultCRUDRepository
{
    public function buscar(int $id)
    {
        $response = parent::find($id);

        if (empty($response)) {
            throw new NotFoundHttpException(
                'Nenhum registro encontrado.',
                null,
                StatusCodeInterface::STATUS_NOT_FOUND
            );
        }

        return $response;
    }

    public function criar(AbstractEntity $entidade)
    {
        $em = parent::getEntityManager();
        $em->persist($entidade);
        $em->flush();
    }

    public function atualizar(AbstractEntity $entidade)
    {
        // TODO: Implement atualizar() method.
    }

    public function deletar(AbstractEntity $entidade)
    {
        // TODO: Implement deletar() method.
    }
}