<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

// Fantasia
$app->get('/fantasia[/{id}]', 'FantasiaController@get');

// Fornecedor
$app->get('/fornecedor[/{id}]', 'FornecedorController@get');
$app->post('/fornecedor', 'FornecedorController@post');